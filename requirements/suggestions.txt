# https://djangopackages.org/grids/
django = {extras = ["argon2"],version = ">=2.2"}
uvicorn
whitenoise
django-rest-assured = "*"
#STATIC MANAGEMENT

black = "*"
pre-commit = "*"
watchdog = "*"

redis = "*"  # cache
django-model-utils = "*"
django-registration

django-environ = "*"
logutils = "*"
django-oauth-toolkit = "*"
djangorestframework-jsonapi = "*"

drf-yasg = {version = "*",extras = ["validation"]}

drf-writable-nested==0.5.1
drf-nested-routers==0.91
