from rest_framework import routers
from .views import ItemView

router = routers.SimpleRouter()
router.register(r"item", ItemView)
urlpatterns = router.urls

# route principal will be api/v1/item/?
# to know all the routes use python manage.py show_urls
