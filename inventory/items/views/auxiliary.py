from rest_framework.decorators import api_view
from rest_framework import status
from inventory.items.models import Item

# https://www.django-rest-framework.org/api-guide/views/#function-based-views


@api_view()
def health(request):
    db_works = Item.objects.exists()
    if db_works:
        return Response({"message": "OK"})  # TODO implement metrics/health standard
    else:
        return Response({"message": "DB not working"}, status=status.HTTP_503_SERVICE_UNAVAILABLE)
        # TODO return as html
    # TODO route it!
