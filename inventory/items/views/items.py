from inventory.items.models import Item
from inventory.items.serializers import ItemSerializer, SimpleItemSerializer
from inventory.items.models.mixins import ErrorMixin

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class ItemView(APIView, ErrorMixin):
    def get_detail(self, pk):
        """GET Detail View"""
        item = Item.objects.get(pk=pk)
        serializer = ItemSerializer(item)
        return Response(serializer.data)

    def retrieve(self):
        """GET List View"""
        item_collection = Item.objects.all()
        serializer = SimpleItemSerializer(item_collection, many=True)
        return Response(serializer.data)

    def create(self, request):
        """POST New Item"""
        serializer = ItemSerializer(data=request.data)  # json to pyobject

        if not serializer.is_valid():
            return self.produce_error(
                serializer.errors, errors.item_creation, errors.item_creation_code
            )

        serializer.save()  # Materialize item
        return Response(serializer.data, status=status.HTTP_201_CREATED)
        # TODO router

    def bulk_create(self):
        """POST api/v1/item/bulk"""

    def update(self):
        """PUT"""
        pass

    def partial_update(self):
        """PATCH"""

    def destroy():
        """DELETE"""
        pass
