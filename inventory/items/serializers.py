from rest_framework import serializers
from inventory.items.models import Item

# https://www.django-rest-framework.org/api-guide/serializers/#hyperlinkedmodelserializer
class FullItemSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Item


class ItemSerializer(FullItemSerializer):

    class Meta(FullItemSerializer.Meta):
        fields = ('id', 'name', 'description', 'price')
    
    
class SimpleItemSerializer(ItemSerializer):

    class Meta(ItemSerializer.Meta):
        exclude = ("price",)
