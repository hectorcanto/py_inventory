from django.db import models
from .common import TrackedModel
from django.contrib.postgres.fields import JSONField


class Item(TrackedModel):  # could be WarehouseItem
    # created_at, updated_at
    id = models.AutoField()  # this is the PK
    # DO NOT USE Autoincrement in Production!!

    name = models.TextField()  # Careful with the DB field
    description = models.TextField()
    stock = models.IntegerField()  # TODO Should be in another table
    # If we have several warehouses we need another table, stock out!

    category = models.TextField()  # TODO will be in another model
    photo = models.ImageField()  # What if many photos!!! TODO
    price = models.IntegerField()  # What about currency, is this production cost
    # Discount?? No, this goes in the checkout process
    # coordenadas: PostGIS
    # w x h x d check for a plugin + volume + sur
    attributes = models.ForeignKey("ItemAttributes")  # Crear un indice, 2 campos, attributes_id
    category = models.ManyToOneRel(related_name="items", on_delete=None)
    # TODO put the name of the intermediate table
    # This gonna make a intermediate table


class Category(TrackedModel):
    id = models.UUIDField()  # this is the PK
    name = models.CharField(unique=True)  # This creates and index
    random_date = models.DateTimeField()
    # items = One2Many

    class Meta:
        unique_together = (("id", "random_date"),)  # has and index!!!

    # Category.objects.count() / 2 * t_scan

    # inventario.es / api / categoria / {name} == comida
