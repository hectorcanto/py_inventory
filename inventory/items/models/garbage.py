class Printable:  # TODO
    def __repr__(self):
        return f"{self.__class__.__name}:{self.pk}"


class Animal(abs.ABC):
    def make_sound(self):
        raise NotImplementedError


class Dog(Animal):
    pass


class Cat(Animal):
    def make_sound(self):
        print("Miau")
