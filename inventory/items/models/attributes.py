from django.db import models
from django.utils.translation import gettext as _
from collections import namedtuple
from dataclasses import dataclass


from django

attributes1 = {}
attributes = dict(weight=0, height=0, width=0, depth=0, size="XL")
# We assume they are boxes of 5 sizes


SIZE = (
    ('S', _('Small')),   # Default value in default language
    ('L', _('Large')),
    ('XL', _('Extra-Large')),
)
# en_US en_UK es_ES in a .po and .mo files

@dataclass(frozen=True)
class Size:
    """
    Esta clase es un envoltorio de valores y logica, no esta pensada para
    instanciarse
    """
     # Esta clase no esta pensada para instanciar, es un wrapper
    XS = 1
    S = 2
    M = 3
    L = 4
    XL = 5
    XXL = 6
    XXXL = 7

    sizes = ["XS", "S", "M", "L", "XL", "XXL", "XXXL"]
    choices = zip(range(1, 8), sizes)
    mapping = ['extra-small', 'small', 'medium', 'large', 'extra-large',
               'extra-extra-large', 'three-x-large']

    @classmethod
    def name(cls, number):
        return cls.mapping[number-1]

Size.name(Size.XS)



class ItemAttributes(models.Model):
    size = models.CharField(default=Size.L, choices=SIZE)
    # attributes = models.JSONField()  # python dict

item =ItemAttributes.objects.get(pk=1)
print(Size.name(item.size))







    # TextField, ArrayField, more options

    # diccionarios
    # propiedades, ojo con DB
    # FK e relaciones
    # Clases "wrapper" exemplo Size.XL + mapping => Django ChoiceField (use a wrapper)
    # buscar plugin weight, TODO


class ItemAttributes(models.Model):
    item_id = models.ForeignKey(Item)
    size
    weight
    # properties





    # TODO add schema.org with a Serializer for SEO
    # OJO Serialize do 3 things: serializer, deserializar e validar, (ETL completa)
    # Tip: GraphQL
