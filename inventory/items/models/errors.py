from common import docs_link

item_creation = f"Error creating Inventory Item, please check docs at {docs_link}"
item_creation_code = 1001
