from rest_framework.response import Response
from rest_framework import status


class ErrorMixin:

    @staticmethod
    def produce_error(error_list: list, message: str, code: int):
        error_body = {
            "errors": error_list,  # Consider API format, f.i. JSON_API
            "message": message,
            "code": code,
        }
        return Response(error_body, status=status.HTTP_400_BAD_REQUEST)
