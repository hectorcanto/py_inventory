from django.db import models


class TrackedModel(models.Model):

    abstract = True

    created_at = models.DateTimeField(auto_now_add=True)  # TODO make it UTC
    updated_at = models.DateTimeField(auto_now=True)
