from django.conf.urls import url

from .views import ItemView

app_name = "items"

urlpatterns = [
    url('^items/', ItemView.as_view(), name="item-collection"),
]
