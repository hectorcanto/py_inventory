from django.apps import AppConfig


class ItemsConfig(AppConfig):
    name = "inventory.items"
    verbose_name = "Ttems"
    # label
