from django.apps import AppConfig


class InfrastructureConfig(AppConfig):
    name = "infrastructure"
    verbose_name = "Infrastructure"
