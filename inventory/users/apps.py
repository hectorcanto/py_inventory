from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = "inventory.users"
    verbose_name = "Users"
