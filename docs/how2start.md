# Install base dependencies

sudo apt install make git

# Install system depencies

git clone $url py_inventory
make install-dependencies

# Create a virtualenv and install Python dependencies

~~~bash
virtualenv -p python3.7 env_py_inventory
source env_py_inventory
pip install -r requirements/dev.txt
~~~

# Recommended IDE packages
Python
Pylint
Markdown