PKGNAME=inventory

GREEN="\\e[32m"
REGULAR="\\e[39m"
RED="\\e[91m"

VERSION=$(shell cat $(PKGNAME)/__init__.py | grep __version | head -n 1 | cut -d" " -f 3| tr -d "'")

show-version:  ## shows current version according to $PKGNAME/__init__.py
	@echo ${VERSION}

tree: PH ## Shortcut to see the folder structure with 'tree'
	tree --dirsfirst -L 3 -I env*

install-dependencies:
	@sudo apt install -y -q --no-install-recommends $(awk '{print $1'} DEPENDENCIES)

clean-py:
	@find . -name '__pycache__' -type d | xargs rm -fr
	@find . -name '*.pyc' -delete
	@find . -name '*.pyo' -delete

clean-build:  ## Remove any build or check artifact, useful for libs
	@rm -rf *.egg-info .sonar .scannerwork build dist .pytest_cache

build:  ## Build Docker image
	docker build -t ${PKGNAME}:dev .

silent-build:  ## Build Docker image silently
	@docker build -q -t ${PKGNAME}:dev .

black:  ## Reformat code black-style
	black -l 100 --exclude 00* ${PKGNAME}

flake8:  ## Pass style check with Flake8, prompting something if everything ok
	@ flake8 \
	&& echo -e "${GREEN}Passed Flake8 style review.${REGULAR}" \
	|| (echo -e "${RED}Flake8 style review failed.${REGULAR}" ; exit 1)

linting:  ## Pass pylint linter, check setup.cfg for conf details
	@pylint --rcfile=setup.cfg ${PKGNAME}/ tests/ | tee .coverage-reports/pylint.txt

version-bump:  ## increase minor version with bump2version
	@python -c 'from ${PKGNAME} import __version__; print("Previous version:", __version__)'
	@bumpversion --allow-dirty minor
	@python -c 'from ${PKGNAME} import __version__; print("New version:", __version__)'

new-version-check:  ## Check if current version already exists (as tag)
	@git ls-remote --exit-code --tags origin $VERSION && echo "Version ${VERSION} already exists!" \
	|| echo "${GREEN}Version ${VERSION} is new. OK.${REGULAR}";

scan-python:  ## Check known vulnerabilities in python packages from PyUp.io SafetyDB
	@safety check --full-report

scan-sonar:  ## Send code and coverage to a sonarqube instance, TODO mount sonarQ locally
	@sed -i -e "s,<source>.*,<source>/usr/src/&{PKGNAME}/</source>,g" .coverage-reports/coverage.xml
	## Patch for issue with route in the coverage plugin
	docker run -v "${PWD}:/usr/src" --user="$(shell id -u):$(shell id -g)" sonarsource/sonar-scanner-cli

scan-docker:  ## Scan for known vulnerabilities with Trivy
	docker run -v /var/run/docker.sock:/var/run/docker.sock aquasec/trivy ${PKGNAME}:dev

# Django
check:  ## Check
	python3 manage.py show_urls
	python3 manage.py check

migrations:  ## Make pending Django migrations
	python3 manage.py makemigrations

apply-migrations:  ## Apply pending migrations
	python3 manage.py migrate

plus:  ## Open a Django Shell Plus terminal
	python3 manage.py shell_plus

resetdb:  ## Reset the DB, will need to create tables again
	python3 manage.py reset_db

urls:  ## Show active Django urls
	python3 manage.py show_urls

help: ## Prompts help for every commad
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	# Issue: include breaks the parsing

.PHONY: clean-py clean-build build silent-build flake flake-docker linting version-bump \
        show-version new-version-check scan-python scan-sonar scan-docker
