#!/usr/bin/env bash
if [ -z $GUNICORN_WORKERS ]; then
    GUNICORN_WORKERS=4
fi

python /opt/inventory/manage.py migrate
python /opt/inventory/manage.py collectstatic --noinput
gunicorn config.wsgi -w $GUNICORN_WORKERS -t 60 -b 0.0.0.0:8000 --chdir=/opt/inventory
