FROM 3.7.7-slim-buster as base

WORKDIR /opt/pip
COPY ./requirements /requirements
RUN pip install --no-cache-dir -r /requirements/prod.txt

# App docker
FROM python:3.8.2-slim-buster
ENV PYTHONUNBUFFERED 1

# copy pip packages including git+ssh
COPY --from=0 /usr/local/lib/python3.7/site-packages /usr/local/lib/python3.7/site-packages
# COPY --from=0 /opt/pip/src/ /opt/pip/src

# copy and enable installed executables
COPY --from=0 /usr/local/bin/ /opt/bin
ENV PATH="${PATH}:/opt/bin"

COPY . /opt/inventory
WORKDIR /opt/inventory/
RUN groupadd -r notroot && useradd -r -u 1000 -g notroot -s /bin/bash notroot  \
    && mkdir -p /var/log/inventory/ && chown -R notroot: /var/log/inventory \
    && chown -R notroot: /opt/inventory \
    && apt-get -qq update && apt-get install -qq --no-install-recommends -y make

# Expose Django's port
# TODO set PORT defautl just in case
EXPOSE $PORT

USER notroot
