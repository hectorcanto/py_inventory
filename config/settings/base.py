import logging.config
from pathlib import Path

import environ
import yaml
from configurations import Configuration


class BaseConfiguration(Configuration):
    # DIRS and Base configuration,
    env = environ.Env()
    ROOT_DIR = Path(__file__).resolve().parents[2]

    LOGGING_CONFIG_FILE = ROOT_DIR / "config" / "logging.yml"
    with open(LOGGING_CONFIG_FILE) as buf:
        log_config = yaml.safe_load(buf)
    logging.config.dictConfig(log_config)

    APPS_DIR = ROOT_DIR / ("inventory")
    ROOT_URLCONF = "config.urls"
    STATIC_ROOT = str(ROOT_DIR / "staticfiles")
    STATIC_URL = "/static/"
    STATICFILES_DIRS = [str(APPS_DIR / "static")]
    STATICFILES_FINDERS = [
        "django.contrib.staticfiles.finders.FileSystemFinder",
        "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    ]

    TEMPLATE_DIRS = (
        APPS_DIR / "templates",
    )

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': TEMPLATE_DIRS,
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]

    # APPS
    LOCAL_APPS = [
        # "inventory.core",  # TODO change to APP_PATH . app
        # "inventory.infrastructure",
        "inventory.users.UsersConfig",
    #    "inventory.items.ItemsConfig",
    ]

    THIRD_PARTY_APPS = [
        "rest_framework",
        "rest_framework.authtoken",
        "django_filters",
        "corsheaders",
        'rest_registration',
        "drf_yasg",
    ]

    DJANGO_APPS = [
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "django.contrib.sessions",
        "django.contrib.sites",
        "django.contrib.messages",
        "django.contrib.staticfiles",
        "django.contrib.admin",
    ]

    # https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
    INSTALLED_APPS = LOCAL_APPS + THIRD_PARTY_APPS + DJANGO_APPS

    # https://docs.djangoproject.com/en/dev/ref/settings/#middleware
    MIDDLEWARE = [
        "debug_toolbar.middleware.DebugToolbarMiddleware",  # TODO remove out in prod
        "corsheaders.middleware.CorsMiddleware",  # extra from default
        "django.middleware.security.SecurityMiddleware",
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
        "django.contrib.messages.middleware.MessageMiddleware",
        "django.middleware.clickjacking.XFrameOptionsMiddleware",
    ]

    # https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
    SECRET_KEY = 'l7af7jzq*qrpq6lzo^q%v*dndf=87*c4$r5pe=gl=a7p*3n7xi'
    # Could be env("DJANGO_SECRET_KEY")
    # TODO change secret per enviroment

    # GENERAL
    DEBUG = env.bool("DJANGO_DEBUG", default=False)
    APPEND_SLASH = True  # https://docs.djangoproject.com/en/dev/ref/settings/#append-slash
    USE_TZ = True
    TIME_ZONE = 'UTC'
    SITE_ID = 1  # https://docs.djangoproject.com/en/dev/ref/settings/#site-id

    LANGUAGE_CODE = 'en-us'
    USE_I18N = True
    USE_L10N = True

    # https://docs.djangoproject.com/en/2.0/ref/settings/#allowed-hosts
    # CORS: https://github.com/ottoyiu/django-cors-headers
    # https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
    ALLOWED_HOSTS = env.list("DJANGO_ALLOWED_HOSTS", default=[])
    CORS_ORIGIN_WHITELIST = env.list("DJANGO_CORS_ORIGIN_WHITELIST", default=[])
    WSGI_APPLICATION = "config.wsgi.application"

    # CACHE
    redis_url = env.str("REDIS_URL", default="redis://redis:6379")
    # TODO set up redis locally or dockerly (expose)

    CACHES = {
        "default": {
            "BACKEND": "django_redis.cache.RedisCache",
            "LOCATION": redis_url,
            "OPTIONS": {
                "CLIENT_CLASS": "django_redis.client.DefaultClient",
                "COMPRESSOR": "django_redis.compressors.lz4.Lz4Compressor",
            },
        }
    }

    # Sessions
    # We are using the cache backend with Redis
    SESSION_ENGINE = "django.contrib.sessions.backends.cache"
    SESSION_CACHE_ALIAS = "default"

    # DATABASES
    # https://docs.djangoproject.com/en/dev/ref/settings/#databases
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'db.sqlite3',
        },
        # TODO move credentials to .env, compose env variables or secrets
        'alternative': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'inventory_db',
            'USER': 'inventory',
            'PASSWORD': 'change_me',
            'HOST': 'localhost',  # Will be the service name in docker
            'PORT': '5432',
        }
    }

    # Auth
    AUTH_USER_MODEL = "users.User"
    LOGIN_URL = "rest_framework:login"  # TODO create namespace
    LOGOUT_URL = "rest_framework:logout"

    AUTHENTICATION_BACKENDS = [  # TODO review
        "django.contrib.auth.backends.ModelBackend",
    ]

    AUTH_PASSWORD_VALIDATORS = [
        {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
        {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', },
        {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator', },
        {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator', },
    ]

    PASSWORD_HASHERS = ["django.contrib.auth.hashers.Argon2PasswordHasher"]
    # TODO change hasher to something simple locally, strong in dev

    # Django Rest Framework
    REST_FRAMEWORK = {
        "DATETIME_FORMAT": "%Y-%m-%dT%H:%M:%S%z",  # TODO move to independent constant for usage
        "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.PageNumberPagination",
        "PAGE_SIZE": int(env("DJANGO_PAGINATION_LIMIT", default=10)),
        "DEFAULT_AUTHENTICATION_CLASSES": [
            "rest_framework.authentication.SessionAuthentication",
            "rest_framework.authentication.TokenAuthentication",
            'rest_framework_simplejwt.authentication.JWTAuthentication',
            # "oauth2_provider.contrib.rest_framework.OAuth2Authentication",
            # "rest_framework_social_oauth2.authentication.SocialAuthentication",
        ],
        "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.IsAuthenticated"],
        "DEFAULT_RENDERER_CLASSES": [
            'rest_framework.renderers.BrowsableAPIRenderer',  # TODO add templates
            "rest_framework.renderers.JSONRenderer"
        ],
        # TODO research versioning in header
        "DEFAULT_VERSIONING_CLASS": "rest_framework.versioning.AcceptHeaderVersioning",
        "DEFAULT_VERSION": "1.0",
    }

    REST_REGISTRATION = {
        'REGISTER_VERIFICATION_ENABLED': False,
        'RESET_PASSWORD_VERIFICATION_ENABLED': False,
        'REGISTER_EMAIL_VERIFICATION_ENABLED': False,

        'REGISTER_VERIFICATION_URL': 'http://127.0.0.1/verify-user/',  # TODO dynamic url
        # VERIFICATION_EMAIL_FROM = "noreplay@local.lan"
        'RESET_PASSWORD_VERIFICATION_URL': 'http://127.0.0.1/reset-password/',
    }
