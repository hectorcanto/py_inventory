from .base import BaseConfiguration


class Test(BaseConfiguration):

    # Debug
    DEBUG = True

    REST_FRAMEWORK = BaseConfiguration.REST_FRAMEWORK
    REST_FRAMEWORK["DEFAULT_RENDERER_CLASSES"] = ["rest_framework.renderers.JSONRenderer", ]
    DATABASES = BaseConfiguration.DATABASES
    DATABASES["default"]["HOST"] = "postgres"  # instead of localhost
