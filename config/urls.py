"""py_inventory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.conf import settings
from django.contrib import admin
from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

V1_ROUTER_PREFIX = r'api/v1/'

app_name = "my_project"

accounts_urls = [
    path('accounts/', include('rest_registration.api.urls')),
]

token_urls = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

drf_login = [
    path('auth/', include('rest_framework.urls', namespace="rest_framework"), name="drf-login"),
]


urlpatterns = [
    # Redirect to API root
    path("", RedirectView.as_view(url=reverse_lazy("api:schema-swagger-ui"),
                                  permanent=False), name="root", ),

    # path(V1_ROUTER_PREFIX, include("config.urls", "root")),  # TODO fix list
    # Admin
    path("admin/", admin.site.urls),

    # Authentication && Users
    path(V1_ROUTER_PREFIX, include(accounts_urls)),
    path(V1_ROUTER_PREFIX, include(drf_login)),
    path(V1_ROUTER_PREFIX, include(token_urls)),

    path(V1_ROUTER_PREFIX, include("inventory.users.urls", "users")),

    # API

    # path(V1_ROUTER_PREFIX, include("inventory.items.urls", "items")),
    # TODO check that users are only visible for superuesr
    # TODO review slash is actually optional
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
